# This is a simple GUI carousel tool for reviewing images/patches.
#
# If run as main, pass a directory and you will be able to review all
# images contained in that directory (png, tiff, jpg).
#
# This is really intended to be embedded into other things that could
# benefit from a quick way to reveiw images (such as DL training
# datasets and stuff), but may require a little bit of extra
# processing to get the image ready.  That's the purpose of using the
# _get_idx method.
#
# Will document more when I'm not on a plane. :) 
#
# Just a heads up that there aren't a lot of guard rails here, so
# reach out if you have any questions.

import sys
import os
import glob

import numpy as np
import matplotlib.pyplot as plt

import imageio

import argparse

import pydicom
from pydicom.fileset import FileSet

class SimpleCarousel():

    idx = 0
    num_images = 0
    ax = None
    artist = None
    dim = 512

    # get_idx and get_label are user-provided functions that provide
    # the carousel with the data it needs to draw. Please see the
    # example_image_carousel implementation for one possible usage.
    #
    # get_idx returns a mpl-displayable matrix
    # get_label returns a string
    def __init__(self, get_idx, get_label, num_images = 1000):
        # Set up the basic drawing tools for the image
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot()
        self.artist = self.ax.imshow(np.zeros([128,128]), cmap="gray", interpolation="none")
        self.im_artist = self.ax.imshow(np.full([512,512], False), animated=True, alpha = 1.0, cmap="gray", interpolation="none")

        self.ax_overlay = self.fig.add_axes(self.ax, xlim=[0,1], ylim=[0,1], position=self.ax.get_position())
        self.ov_artist = self.ax_overlay.text(10, 10, "halloooooo", color="white", backgroundcolor="black", verticalalignment="top")
  
        self._get_idx = get_idx
        self._get_label = get_label
        self.num_images = num_images

        plt.rcParams["keymap.save"].remove('s')
        plt.rcParams["keymap.quit"].remove('q')
        plt.rcParams["keymap.fullscreen"].remove('f')
        self.fig.canvas.mpl_connect("key_press_event", self.on_press)
        #self.fig.canvas.mpl_connect("scroll_event", on_scroll)
        self.draw()
        plt.show()

    # All the event handlers
    def next(self):
        self.idx = min(self.idx+1, self.num_images-1)
        self.draw()
        
    def prev(self):
        self.idx = max(self.idx-1, 0)
        self.draw()

    def draw(self):
        # Update the image
        im = self._get_idx(self.idx)
        self.im_artist.set_data(im)
        self.im_artist.set_clim([-1200,200])
        self.ax.draw_artist(self.im_artist)

        # update the label
        lab = self._get_label(self.idx)
        self.ov_artist.set_text(lab)
        self.ax.draw_artist(self.ov_artist)

        self.fig.canvas.blit(self.fig.bbox)
        self.fig.canvas.flush_events()

    def on_press(self, event):
        print(event.key)
        if event.key == "f" or event.key=="right":
            self.next()
        elif event.key == "b" or event.key=="left":
            self.prev()
        elif event.key == "q":
            sys.exit(0)
        elif event.key == "p":
            pass
            
# Slightly forgiving (will skip files if they don't parse correctly.)
# Will load everythign though so it won't bother trying to distinguish
# between series and stuff.  I'm not a wizard.
def dicom_carousel(img_dir):
    
    file_list = []
    
    listing = os.listdir(img_dir)
    
    files = []
    for elem in listing:
        full_path = os.path.join(img_dir, elem)
        
        if os.path.isfile(full_path):
            files.append(full_path)

    datasets = []
    for curr_file in files:
        print(curr_file)
        try:
            ds = pydicom.dcmread(curr_file)
            datasets.append(ds)
        except Exception as e:
            #print(e)
            print(f"Skipping {curr_file} (pydicom failed to parse)")
            pass

    # Sort by instance number
    def sort_func(e):
        return e.InstanceNumber
    
    datasets.sort(key=sort_func)
    
    def get_idx(idx):
        r_slope = datasets[idx].RescaleSlope
        r_intercept = datasets[idx].RescaleIntercept
        return r_slope*datasets[idx].pixel_array + r_intercept

    def get_label(idx):
        return f"Current: {datasets[idx].InstanceNumber}\nZ Loc: {datasets[idx].SliceLocation}"

    SimpleCarousel(get_idx, get_label, num_images = len(datasets))

def example_image_carousel(img_dir):

    img_formats = ["*.png", "*.jpg", "*.tiff"]

    os.path.join(img_dir, "")

    file_list = []
    for fmt in img_formats:
        glob_path = os.path.join(img_dir, fmt)
        files = glob.glob(glob_path)        
        file_list = file_list + files

    file_list.sort()
    
    print(file_list)

    def get_idx(idx):
        path = file_list[idx]
        img = imageio.imread(path)
        arr = np.asarray(img)
        return np.squeeze(img)

    def get_label(idx):
        return f"Current: {file_list[idx]}\nIndex: {idx}"

    SimpleCarousel(get_idx, get_label, num_images = len(file_list))
            
if __name__=="__main__":

    ap = argparse.ArgumentParser(prog="viewer", description="an image viewer with less bullshit", epilog="do what you can to make the world better")

    ap.add_argument("--dcm", "--dicom", action="store_true")
    ap.add_argument("--png", action="store_true")
    ap.add_argument("--ext")    
    ap.add_argument('imgdir', nargs=argparse.REMAINDER, default=os.getcwd())


    args = ap.parse_args()

    print(args.dcm)
    print(args.png)
    print(args.ext)
    print(args.imgdir)

    if len(args.imgdir) > 1:
        print(ap.usage())

    img_dir = args.imgdir[0]

    if args.dcm:
        dicom_carousel(args.imgdir[0])

    elif args.png:
        example_image_carousel(img_dir)
    

