# Less Bad Tools

I feel a deep dissatisfaction with the currently tools available to us as imaging researchers.  No one builds things for us (there's no money in it, I guess?) so we're stuck using over-engineered, un-free software.

Everything here is licensed with the following: do what thou wilt. See the [license](LICENSE) for more details.

I'd appreciate a shout out or attribution of you find these valuable, or want to create a derivative version, but that's not required.  Even just a private message letting me know that you've found any of this work useful is appreciated. 

